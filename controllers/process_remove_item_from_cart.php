<?php
	//the goal is to remove an item from the variable $_SESSION['cart']
	//We need an identifier to know which item we will remove
	// [itemId=>quantity, itemId=>quantity]
	session_start();

	$item_id = $_GET['item_id'];

	unset($_SESSION['cart'][$item_id]);

	header("Location: " . $_SERVER['HTTP_REFERER']);
?>