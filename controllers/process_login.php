<?php
    require "connection.php";
    session_start();
    
    
    $email = $_POST['email'];
    $password = $_POST['password'];

    // we'll create a query searching for an entry with captured email

    $user_query = "SELECT * FROM users WHERE email = '$email'";
    $user = mysqli_query($conn, $user_query);
    $user_info = mysqli_fetch_assoc($user);
    
    if(mysqli_num_rows($user) === 1){
        // chech the password from the input if equal to the password in the datbase
        // but remember, the password in the database is hashed
        if(password_verify($password, $user_info['password'])){
            $_SESSION['user'] = $user_info;
            header("Location: ../index.php");
            die("login successful");

        }else{
            die("login failed, wrong password!");
        }
    }else{
        die("login failed, user not found");
    }

?>