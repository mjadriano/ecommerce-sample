<?php 
    session_start();
    //  we need to remove all session variables
    session_unset();
    // we need to destroy the session
    session_destroy();
    // redirect to catalog
    header("Location: ../index.php");

?>