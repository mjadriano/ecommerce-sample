<?php
$host = "localhost";  //this is our current host
$db_username = "root"; //this is the usernameof our db
$db_password = ""; //password for the host
$db_name = "b67Ecommerce"; //name of our database

//create the connection
$conn = mysqli_connect($host, $db_username, $db_password, $db_name);


//check if the connection is successful
if(!$conn){
    die("Connection Failed: " . mysqli_error($conn));
}


?>