<?php
    require "template/template.php";

    function getTitle(){
        echo "LanceGo | Add Profile";
    }

    function getContent(){
        
        ?>

<!-- Add profile for the users -->

<h1 class="text-center mt-5">Profile</h1>
<div class="d-flex justify-content-center align-items-center">
    <form action="controllers/process_add_profile.php" method="POST" class="mb-5" enctype="multipart/form-data">
        <div class="form-group">
            <input type="text" class="form-control" name="address" placeholder="Input your address">
        </div>
        <div class="form-group">
            <input type="text" class="form-control mb-2" name="contactNumber" placeholder="Contact Number">
        </div>
        <div class="form-group">
            <input type="file" name="profileImage">
        </div>
        <button class="btn btn-info" type="submit">Submit</button>

    </form>
</div>


<?php
// var_dump($_SESSION['user']['id']);
}
?>