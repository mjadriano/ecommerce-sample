<?php 

    require "template/template.php";
    
    
    function getTitle(){
        echo"LanceGo";
    }


    function getContent(){
    // in here we want to publish the content of the index.php
        //We need to call the connection file whenever we need to access the db
        require "controllers/connection.php";

        ?>
<!-- Catalog Body -->
<div class="container">
    <!-- <?php var_dump($_SESSION['cart']); ?> -->

    <div class="row">

        <!-- Sort -->
        <div class="col-lg-2">
            <h3 class="py-5">Categories</h3>
            <ul class="list-group border">
                <li class="list-group-item">
                    <a href="index.php">All</a>
                </li>

                <!-- List of all categories in li -->

                <?php
                $category_query = "SELECT * FROM categories";
                $categories = mysqli_query($conn, $category_query);
                foreach($categories as $indivCategory){
                    ?>
                <li class="list-group-item">
                    <a
                        href="index.php?category_id=<?php echo $indivCategory['id'] ?>"><?php echo $indivCategory['name']; ?></a>
                </li>
                <?php
                }
                
                ?>
            </ul>
            <h3 class="py-5">Sort By</h3>
            <ul class="list-group border">
                <li class="list-group-item">
                    <a href="controllers/process_sort.php?sort=asc">Price (Lowest to Highest</a>
                </li>
                <li class="list-group-item">
                    <a href="controllers/process_sort.php?sort=desc">Price (Highest to Lowest)</a>
                </li>
            </ul>
        </div>

        <!-- Catalog Cards -->
        <div class="col-lg-10">
            <h1 class="text-center display-3 my-5">Catalog
            </h1>
            <div class="row">

                <!-- We need to access the db to get the list items -->
                <?php
                       


                        //We will now get the items from db
                        //1. creat the query
                        $items_query = "SELECT * FROM items";
                        // Next goal: if we gave a category_id in the url, we will filter the items query to ony get the items
                        // with category_id = to the vaue in the url
                        // $_GET --- this gets the data from the url

                        if(isset($_GET['category_id'])){
                            $categoryId = $_GET['category_id'];
                            
                            // if category_id exists, we wil concatenate WHERE category_id = $categoryID to the exsting items query.
                            // .= concatenation-assignment operator.

                            $items_query .= " WHERE category_id = $categoryId";


                        }

                       
                        //this is to add the data from the session if the session exists
                        if(isset($_SESSION['sortDataFromSession'])){
                            // var_dump($_SESSION);
                            $items_query .= $_SESSION['sortDataFromSession'];
                            
                        }

                        //2. access the db via mysqli_query
                        $items = mysqli_query($conn, $items_query);
                        
                        foreach($items as $indivItem){
                            ?>
                <div class="col-lg-4 py-2">
                    <div class="card">
                        <img src="<?php echo $indivItem['imgPath']?>" alt="" class="card-img-top" height="200px">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $indivItem['name'] ?></h4>
                            <p class="card-text">Php<?php echo $indivItem['price'] ?></p>
                            <p class="card-text">Item Descriotion: <?php echo $indivItem['description'] ?></p>
                            <p class="card-text">Quantity: <?php echo $indivItem['quantity'] ?></p>
                            <?php 
                                //we want to publis the category name. so far, we only have the category_id. what we can do is to look for the category row where id = category_id;
                                
                                $categoryId= $indivItem['category_id'];

                                $category_query =  "SELECT * FROM categories WHERE id = $categoryId";
                                
                                $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
                                
                                //!!! whenever we are retrieving a single a row, we need to transform the result into an associative aaray for us to be able to use the result.
                                 
                                ?>
                            <p class="card-text">Category: <?php echo $category['name']; ?> </p>
                        </div>
                        <div class="card-footer">
                            <!-- will refractor this part later on -->
                            <a href="controllers/process_delete_item.php?item_id=<?php echo $indivItem['id'] ?>"
                                class="btn btn-danger">Delete Item</a>

                            <a href="edit-item.php?item_id=<?= $indivItem['id'] ?>" class="btn btn-info">Edit Item</a>
                        </div>
                        <div class="card-footer">
                            <form action="controllers/process_add_to_cart.php" method="POST">
                                <input type="number" class="form-control" name="quantity">
                                <input type="hidden" name="item_id" value="<?php echo $indivItem['id']; ?>">
                                <input type="hidden" name="quantity_from_db"
                                    value="<?php echo $indivItem['quantity']; ?>">
                                <input type="hidden" name="item_name" value="<?php echo $indivItem['name']; ?>">
                                <button type="button" class="btn btn-info addToCart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>

                <?php
                        }
                    ?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>