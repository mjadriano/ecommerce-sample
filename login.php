<?php
    require "template/template.php";


    function getTitle(){
        echo "LanceGo | Register";
    }

    function getContent(){
?>

<div class="d-flex justify-content-center align-items-center flex-column">
    <h1 class=py-5>Login</h1>
    <form action="controllers/process_login.php" method="POST">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
    <p class="py-3">New user? <a heref="register.php">Register </a></p>
</div>
<?php

    }
        ?>