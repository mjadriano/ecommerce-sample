<?php
    require "template/template.php";

    function getTitle(){
        echo "LanceGo | Cart";
    }

    function getContent(){
        require "controllers/connection.php";
        ?>
<h1 class="text-center py-5">Cart Page</h1>
<hr>

<div class="table-responsive col-lg-10 offset-lg-1">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Item</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php

            $total = 0;
        // we need to get all the items from the session
        //magchecheck muna kung may laman ang cart
        if(isset($_SESSION['cart'])){
            //REMEBER HOW TO FOREACH ASSOCIATIVE ARRAY PLEASE!!
            foreach($_SESSION['cart'] as $item_id => $quantity){
        //we need to get the details of the item (name, price)
        //we need to check the data from our db
        $item_query = "SELECT * FROM items WHERE id = $item_id";
        $item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
        //since we alrady know the price, we can get the subtotal by multiplying qty * price
        $subtotal = $quantity * $item['price'];

        //This is to add the subtotal of an item to the total amount
        $total += $subtotal;
                ?>
            <tr>
                <td><?php echo $item['name']; ?></td>
                <td><?php echo $item['price']; ?></td>
                <td><?php echo $quantity; ?></td>
                <td><?php echo $subtotal; ?></td>
                <td>
                    <a href="controllers/process_remove_item_from_cart.php?item_id=<?php echo $item['id']; ?>"
                        class="btn btn-danger">Remove</a>
                </td>
            </tr>
            <?php
            }
        }
        
        
        //since we already know the subtotal, we can get the total amount of the cart
        

        
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total: <?php echo $total; ?></td>
                <td>
                    <a href="controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a>
                </td>
            </tr>
        </tbody>

    </table>

</div>


<?php
    }



?>