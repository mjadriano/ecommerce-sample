<?php
require "template/template.php";

function getTitle()
{
    echo "LanceGo| Update Profile";
}

function getContent()
{

    require "controllers/connection.php";
    if (isset($_SESSION['user'])) {
        $userId = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles WHERE user_id =  $userId";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
        $user_query = "SELECT * FROM users WHERE id =  $userId";
        $user = mysqli_fetch_assoc(mysqli_query($conn, $user_query));

?>

<h1 class="text-center py-5">Update Profile</h1>
<div class="d-flex justify-content-center align-items-center">
    <form action="controllers/process_update_profile.php" method="POST" class="mb-5" enctype="multipart/form-data">
        <div class="form-group">
            <label for="firstName">First Name</label>
            <input type="text" name="firstName" class="form-control" value="<?php echo $user['firstName']; ?>">
        </div>
        <div class="form-group">
            <label for="lastName">Last Name</label>
            <input type="text" name="lastName" class="form-control" value="<?php echo $user['lastName']; ?>">
        </div>
        <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" name="email" class="form-control" value="<?php echo $user['email']; ?>">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type=" text" name="address" class="form-control" value="<?php echo $profile['address']; ?>">
        </div>
        <div class="form-group">
            <label for="contactNo">Contact Number</label>
            <input type=" text" name="contactNo" class="form-control" value="<?php echo $profile['contactNo']; ?>">
        </div>
        <div class="form-group">
            <label for="profileImg">Profile Picture</label>
            <img src="<?php echo $profile['profileImg'] ?>" height="50px" width="50px" alt="">
            <input type="file" name="profileImg" class="form-control">
        </div>

        <button class="btn btn-success" type="submit">Update Profile</button>
    </form>
</div>


<?php
    }
}
?>