<?php
    require "template/template.php";

    function getTitle(){
        echo "LanceGo | Edit Item";
    }
    function getContent(){
        require "controllers/connection.php";

        // get the item id
        $itemId = $_GET['item_id'];

        $item_query = "SELECT * FROM items WHERE id = $itemId";

        // transform the result of the query into an associative array
        $item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

       ?>

<!-- Add form content  -->
<h1 class="text-center py-5">Edit Item Form</h1>
<div class="d-flex justify-content-center align-items-center ">
    <!-- We need to remember, if we want to capture data from an input type file, we need to add the attribute enctype="multipart/form-data" in our form tag-->

    <form action="controllers/process_edit_item.php" method="POST" class="mb-5" enctype="multipart/form-data">
        <div class="form-gourp">
            <label for="name">Item Name:</label>
            <input type="text" name="name" class="form-control" value="<?php echo $item['name'] ?>">
        </div>
        <div class=" form-group">
            <label for="price">Item Price</label>
            <input type="number" name="price" class="forn-control" value="<?php echo $item['price'] ?>">

        </div>
        <div class="form-group">
            <label for="quantity">Item Quantity: </label>
            <input type="number" name="quantity" class="form-control" value="<?php echo $item['quantity'] ?>">

        </div>
        <div class="form-group">
            <label for="description">Item Description</label>
            <textarea name="description" class="form-control"><?php $item['description'] ?></textarea>

        </div>

        <!-- for image -->
        <div class="form-group">
            <label for="imgPath">Item Image</label>
            <img src="<?php echo $item['imgPath'] ?>" height="50px" width="50px" alt="">
            <input type="file" name="imgPath" class="form-control">

        </div>

        <!-- for the category -->
        <div class="form-group">
            <label for="category_id">Category: </label>
            <select name="category_id" class="form-control">
                <?php
    require "controllers/connection.php";

    $category_query = "SELECT * FROM categories";
    $categories = mysqli_query($conn, $category_query);


    foreach($categories as $indivCategory){
        // what we want is to check if indivCategory[id] = item[ategory_id]
        // if equal print "selected"
        // if-elese || tenary operator
        // (condition) ? (do this if true) : (do this if false)

        ?>
                <option value="<?php echo $indivCategory['id'] ?>"
                    <?php echo $indivCategory['id'] === $item['category_id'] ? "selected" : "" ?>>
                    <?php echo $indivCategory['name'] ?></option>


                <?php
    }
?>
            </select>

        </div>
        <!-- this input is for us to get the id of the item we are editing. this is not meant to be edited by the user. -->
        <input type="hidden" name="item_id" value="<?php echo $item['id'] ?>">
        <button class="btn btn-info" type="submit">Update item</button>
    </form>
</div>
<?php
    }
?>