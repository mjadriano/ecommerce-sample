<?php
    require "template/template.php";

    function getTitle(){
        echo "LanceGo | Add Profile";
    }

    function getContent(){
        require "controllers/connection.php";


        $nameId = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles WHERE user_id = $nameId";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
        $user_query = "SELECT * FROM users WHERE id =  $nameId";
        $user = mysqli_fetch_assoc(mysqli_query($conn, $user_query));
        
        

if(!$profile){
    ?>
<div class="d-flex align-items-center justify-content-center flex-column vh-100">
    <h1 class="">Please update your profile </h1>
    <a href="add_profile.php" class="btn btn-success">here!</a>
</div>
<?php
}else{
    ?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="text-center py-5">Your Profile</h1>

            <div class="text-center py-3">
                <img src="<?php echo $profile['profileImg']; ?>" height=" 200px" class="rounded-circle" alt="">
            </div>
            <table class="table table-striped">
                <tr>
                    <td>First Name</td>
                    <td><?php echo $user['firstName']; ?></td>
                </tr>

                <tr>
                    <td>Last Name</td>
                    <td><?php echo $user['lastName']; ?></td>
                </tr>

                <tr>
                    <td>Email Address</td>
                    <td><?php echo $user['email']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><?php echo $profile['address']; ?></td>
                </tr>

                <tr>
                    <td>Contact Number</td>
                    <td><?php echo $profile['contactNo']; ?></td>
                </tr>
            </table>

            <a href="update_profile.php" class="btn btn-info mb-3">Update Profile</a>
            <a href="controllers/process_delete_profile.php?item_id=<?= $profile['id'] ?>" class="btn
                    btn-danger mb-3">Delete Profile</a>
        </div>
    </div>
</div>


<?php
}
?>




<?php
        }
        ?>