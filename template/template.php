<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootswatch -->
    <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css">

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>



    <!-- IF a file is a dependency of another file, it should be at the top -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" defer></script>

    <!-- toastr css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


    <!-- Javascript link -->
    <!-- Registration Validation Script -->
    <script src="../assets/scripts/register.js" defer></script>

    <!-- Add to cart js -->
    <script src="../assets/scripts/addToCart.js" defer></script>


    <title><?php getTitle(); ?></title>
</head>

<body>

    <?php require"navbar.php"; ?>

    <!-- This is where we want to insert the conetent of the pages -->
    <!-- we will create a function in every page, to get the content of that page -->

    <?php
        getContent();


    ?>

    <?php require"footer.php"; ?>


</body>

</html>