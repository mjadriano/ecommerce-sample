//we need to get all add to cart btns
//we need to add an event listener to each of the btn
// validate the quantity
// if the quantity is correct, send data via fetch

const addToCartBtns = document.querySelectorAll(".addToCart");

addToCartBtns.forEach(function (indivAddToCartBtn) {
  indivAddToCartBtn.addEventListener("click", function (indivBtn) {
    //indidBtn.target represents the actual button that was clicked
    const quantity =
      indivBtn.target.parentElement.firstElementChild.nextElementSibling.value;
    const quantityFromDb =
      indivBtn.target.previousElementSibling.previousElementSibling.value;
    const itemName = indivBtn.target.previousElementSibling.value;
    const itemId =
      indivBtn.target.parentElement.firstElementChild.nextElementSibling.value;

    if (quantity <= 0) {
      toastr["error"]("Please input correct quantity.");
    } else if (parseInt(quantity) > parseInt(quantityFromDb)) {
      toastr["error"]("Insufficient Stocks");
    } else {
      const data = new FormData();

      data.append("item_id", itemId);
      data.append("quantity", quantity);
      data.append("quantity_from_db", quantityFromDb);

      fetch("../../controllers/process_add_to_cart.php", {
        method: "POST",
        body: data,
      })
        .then(function (response) {
          return response.text();
        })
        .then(function (response_from_fetch) {
          console.log(response_from_fetch);
          if (response_from_fetch === "insufficient") {
            toastr["error"]("Not enough stocks");
          } else {
            toastr["success"](
              "Successfully added ${quantity} ${itemName} to cart."
            );
          }
        });
    }
  });
});
