// if email is unique
// no empty fields
// alphanumeric password 1 Capital Letter, 1 number, 1 symbol
// password more than 8
// valid email
// (ok)no number in firstName/ (ok)lastName
// password and confirm password are the same
// console.log("Hello!");
//disable button
const registerBtn = document.getElementById("registerBtn");
registerBtn.disabled = true;

const firstNameInput = document.getElementById("firstName");
const lastNameInput = document.getElementById("lastName");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const confirmPasswordInput = document.getElementById("confirmPassword");

function checkIfLetters(value, element) {
  const letters = /^[A-Za-z]+$/;
  if (!value.match(letters)) {
    element.nextElementSibling.textContent =
      "Numbers and Special Characters are not allowed";
  } else {
    element.nextElementSibling.textContent = "";
  }
}

function isEmpty(value, element) {
  if (value === "") {
    element.nextElementSibling.textContent = "This field is required.";
    return true;
  } else {
    element.nextElementSibling.textContent = "";
    return false;
    // checkIfLetters(value, element);
  }
}

function enableButton() {
  if (
    firstNameInput.value === "" ||
    lastNameInput.value === "" ||
    emailInput.value === "" ||
    passwordInput.value === "" ||
    passwordInput.value !== confirmPasswordInput.value
  ) {
    registerBtn.disabled = true;
  } else {
    registerBtn.disabled = false;
  }
}

firstNameInput.addEventListener("blur", function () {
  const firstNameValue = firstNameInput.value;
  //validation using regex
  if (!isEmpty(firstNameValue, firstNameInput)) {
    checkIfLetters(firstNameValue, firstNameInput);
  }
  enableButton();
});

lastNameInput.addEventListener("blur", function () {
  const lastNameValue = lastNameInput.value;
  if (!isEmpty(lastNameValue, lastNameInput)) {
    checkIfLetters(lastNameValue, lastNameInput);
  }
  enableButton();
});

emailInput.addEventListener("blur", function () {
  const lastNameValue = lastNameInput.value;
  isEmpty(lastNameValue, lastNameInput);
});

emailInput.addEventListener("blur", function () {
  const emailValue = emailInput.value;
  isEmpty(emailValue, emailInput);
  enableButton();
});

passwordInput.addEventListener("blur", function () {
  const passwordValue = passwordInput.value;
  if (!isEmpty(passwordValue, passwordInput)) {
    const letters = /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^\w\s]).{8,32}$/;
    if (!letters.test(passwordValue)) {
      passwordInput.nextElementSibling.innerHTML =
        "Password must have at least 1 uppercase letter, 1 lowercase" +
        "letter, 1 special character, and a number";
    } else {
      passwordInput.nextElementSibling.textContent = "";
    }
  }
  enableButton();
});

confirmPasswordInput.addEventListener("blur", function () {
  const confirmPasswordValue = confirmPasswordInput.value;
  if (!isEmpty(confirmPasswordValue, confirmPasswordInput)) {
    if (confirmPasswordValue !== passwordInput.value) {
      confirmPasswordInput.nextElementSibling.textContent =
        "Passwords should match";
    } else {
      confirmPasswordInput.nextElementSibling.textContent = "";
    }
  }
  enableButton();
});

registerBtn.addEventListener("click", function () {
  //This will serve as our form data container. Since we disabled the form submission in our fronted, we need a
  //container to hold the data that can be accessed by $_POST
  let data = new FormData();
  //to add a data in our newly created FormData, use append method, the first arg is the name whoch is equivalent

  data.append("firstName", firstNameInput.value);
  data.append("lastName", lastNameInput.value);
  data.append("email", emailInput.value);
  data.append("password", passwordInput.value);
  //if we will not get data (GET)
  //since we want to add a data in our database, the second argument will be an object contining the method and the data we want to add or process
  fetch("../../controllers/process_register.php", {
    method: "POST",
    body: data,
  })
    .then(function (response) {
      //the response parameter is the response from the fetch.
      //we need to transform it into a data format that we can use, for this instance, we transformed it into a text format.
      return response.text();
    })

    .then(function (response_from_fetch) {
      //the respomse_from_fetch is the data we got from the firts then which is the response.text()
      console.log(response_from_fetch);
      if (response_from_fetch === "duplicate") {
        toastr["warning"]("Email already exists");
      } else {
        //we are redirecting to the login page
        window.location.replace("../../login.php");
      }
    });
});
