<?php
    require "template/template.php";


    function getTitle(){
        echo "LanceGo | Register";
    }

    function getContent(){
        ?>
<div class="d-flex justify-content-center flex-column align-items-center">
    <h1 class="py-5">Register</h1>
    <form action="controllers/process_register.php" method="POST">
        <div class="form-group">
            <label for="firstName"><span class="text-danger">*</span>First Name:</label>
            <input type="text" name="firstName" class="form-control" id="firstName">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="lastName"><span class="text-danger">*</span>Last Name:</label>
            <input type="text" name="lastName" class="form-control" id="lastName">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="email"><span class="text-danger">*</span>Email</label>
            <input type="email" name="email" class="form-control" id="email">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="password"><span class="text-danger">*</span>Password:</label>
            <input type="password" name="password" class="form-control" id="password">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="confirmPassword">
                <span class="text-danger">*</span>Confirm Password:
            </label>
            <input type="password" name="confirmPassword" class="form-control" id="confirmPassword">
            <span class="text-danger"></span>
        </div>

        <button type="button" class="btn btn-info" id="registerBtn">Register</button>
    </form>
    <p class="py-2"> Already Registered? <a href="login.php">Login</a></p>

</div>


<?php
    }

?>